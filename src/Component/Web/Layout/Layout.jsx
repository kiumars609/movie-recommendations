import React from "react";
import CenterSide from "./CenterSide/CenterSide";
import Footer from "./Footer/Footer";
import LeftSide from "./LeftSide/LeftSide";
import RightSide from "./RightSide/RightSide";
import "./scss/layout.css";

export default function Layout({ children }) {
  return (
    <>
      <div className="container-fluid col-md-12 d-flex mb-5">
        <LeftSide />
        <CenterSide>
            {children}
        </CenterSide>
        <RightSide />
        {/* <Footer /> */}
      </div>
    </>
  );
}
