import React from "react";

export default function CenterSide({ children }) {
  return (
    <>
      <div className="center-panel col-md-8">
        <nav className="col-md-12 nav">
          <ul>
            <li>
              <a href="#">Movies</a>
            </li>
            <li>
              <a href="#">Series</a>
            </li>
            <li>
              <a href="#">TV Shows</a>
            </li>
            <li>
              <a href="#">Animations</a>
            </li>
            <li>
              <a href="#">Plans</a>
            </li>
          </ul>
        </nav>
        {children}
      </div>
    </>
  );
}
