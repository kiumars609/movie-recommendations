import React from "react";

export default function RightSide() {
  return (
    <>
      <div className="right-panel col-md-2">
        <div className="col-md-12 account-manage">
          <div className="col-md-12 login d-flex">
            <img
              src="/assets/image/panel/avatar/admin2.webp"
              className="avatar"
            />
            <nav className="navbar navbar-expand-lg navbar-light">
              <ul className="navbar-nav me-auto mb-2 mb-lg-0">
                <li className="nav-item dropdown">
                  <a
                    className="nav-link dropdown-toggle user-name"
                    href="#"
                    id="navbarDropdown"
                    role="button"
                    data-bs-toggle="dropdown"
                    aria-expanded="false"
                  >
                    Anahita
                  </a>
                  <ul
                    className="dropdown-menu"
                    aria-labelledby="navbarDropdown"
                  >
                    <li>
                      <a className="dropdown-item" href="#">
                        User Panel
                      </a>
                    </li>
                    <li>
                      <hr className="dropdown-divider" />
                    </li>
                    <li>
                      <a className="dropdown-item" href="#">
                        Logout
                      </a>
                    </li>
                  </ul>
                </li>
              </ul>
            </nav>
          </div>
        </div>

        <nav className="nav mt-4">
          <ul className="position-relative media-services">
            <span>Media Service</span>
            <li>
              <a href="#">
                <img src="/assets/image/icon/apple-tv.png" /> Apple TV +
              </a>
            </li>
            <li>
              <a href="#">
                <img src="/assets/image/icon/hbo.png" /> HBO Max
              </a>
            </li>
            <li>
              <a href="#">
                <img src="/assets/image/icon/peacock.png" /> Peacock
              </a>
            </li>
            <li>
              <a href="#">
                <img src="/assets/image/icon/disney.png" /> Disney +
              </a>
            </li>
            <li>
              <a href="#">
                <img src="/assets/image/icon/hulu.png" /> Hulu
              </a>
            </li>
            <li>
              <a href="#">
                <img src="/assets/image/icon/netflix.png" /> Netflix
              </a>
            </li>
            <li>
              <a href="#">
                <img src="/assets/image/icon/prime.png" /> Prime
              </a>
            </li>
            <hr />
          </ul>
        </nav>

        <nav className="nav">
          <ul className="position-relative">
            <span>Genre</span>
            <li>
              <a href="#">Action</a>
            </li>
            <li>
              <a href="#">Comedy</a>
            </li>
            <li>
              <a href="#">Drama</a>
            </li>
            <li>
              <a href="#">Thriller</a>
            </li>
            <li>
              <a href="#">Western</a>
            </li>
            <li>
              <a href="#">Horror</a>
            </li>
            <li>
              <a href="#">Sci-Fi</a>
            </li>
          </ul>
        </nav>
      </div>
    </>
  );
}
