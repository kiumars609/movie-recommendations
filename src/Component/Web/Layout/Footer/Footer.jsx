import React from "react";

export default function Footer() {
  return (
    <>
      <footer>
        <button className="footer-login-btn btn">
          Sign in for more access
        </button>
      </footer>
    </>
  );
}
