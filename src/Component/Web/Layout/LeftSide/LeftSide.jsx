import React from "react";

export default function LeftSide() {
  return (
    <>
      <div className="left-panel col-md-2">
        <img src="/assets/image/logo.png" className="m-3" />
        <nav className="nav">
          <ul className="position-relative">
            <span>Menu</span>
            <li>
              <a href="#">
                <i className="fa fa-home" aria-hidden="true"></i> Home
              </a>
            </li>
            <li>
              <a href="#">
                <i className="fa fa-compass" aria-hidden="true"></i> Discovery
              </a>
            </li>
            <li>
              <a href="#">
                <i className="fa fa-users" aria-hidden="true"></i>Community
              </a>
            </li>
            <li>
              <a href="#">
                <i className="fa fa-clock" aria-hidden="true"></i> Coming soon
              </a>
            </li>
            <hr />
          </ul>
        </nav>

        <nav className="nav">
          <ul className="position-relative">
            <span>Library</span>
            <li>
              <a href="#">
                <i className="fa fa-clock" aria-hidden="true"></i>Recent
              </a>
            </li>
            <li>
              <a href="#">
                <i className="fa fa-bookmark" aria-hidden="true"></i> Bookmarked
              </a>
            </li>
            <li>
              <a href="#">
                <i className="fa fa-star" aria-hidden="true"></i>Top Rated
              </a>
            </li>
            <li>
              <a href="#">
                <i className="fa fa-download" aria-hidden="true"></i>
                Downloaded
              </a>
            </li>
            <hr />
          </ul>
        </nav>

        <nav className="nav">
          <ul className="position-relative">
            <li>
              <a href="#">
                <i className="fa fa-cog" aria-hidden="true"></i> Settings
              </a>
            </li>
            <li>
              <a href="#">
                <i className="fa fa-exclamation-circle" aria-hidden="true"></i>{" "}
                Help
              </a>
            </li>
          </ul>
        </nav>
      </div>
    </>
  );
}
