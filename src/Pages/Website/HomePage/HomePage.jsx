import React from "react";
import CarouselProduct from "../../../Component/CarouselProduct/CarouselProduct";
import Layout from "../../../Component/Web/Layout/Layout";
import "./scss/homePage.css";
import Slider from "./Slider/Slider";

export default function HomePage() {

  return (
    <>
      <Layout>
        {/* Start Slider */}
        <Slider />
        {/* End Slider */}

        {/* Start New Trailer */}
        <div className="col-md-12 new-trailer mt-5">
          <h3 className="original-title mb-3">New Trailers</h3>

          <div
            className="card"
            style={{
              backgroundImage: `url(/assets/image/trailer/spidey.jpg)`,
              objectFit: "cover",
              backgroundSize: "100%",
            }}
          >
            <div className="card-body">
              <span>
                <h5 className="card-title">
                  SpiderMan Across the Spider-Verse
                </h5>
                <p href="#" className="card-link">
                  2023
                </p>
              </span>
            </div>
          </div>

          
        </div>
        {/* Start New Trailer */}


        



        <CarouselProduct />
      </Layout>
    </>
  );
}
